# constants ############################################################################################################
R_PAR = 10000 # input resistor of the buffer stage, single ended
U_INPUT = 1.7 # ADC has input sensitivity of 1.7V because it uses a gain of 6Db in its buffer stage
DB_OFFSET = 0.5 # actual divided voltage range must be 0.5dB higher than actual measured target voltage
TARGET_VOLTAGES = [2.1, 2.9, 4.2, 5.461986047, 7.117133334, 10.5]


# helper functions #####################################################################################################
def dbToGainFactor(dbValue):
    return pow(10, dbValue / 20.0)


# E-series Rs ##########################################################################################################
N = 96 # E-Range for resistors
E_RESISTOR_SERIES = []
for decade in (1000, 10000, 100000):
    for m in range(0, N):
        factor = round((10 ** m) ** (1 / N), 2)
        E_RESISTOR_SERIES.append(factor * decade)


# main program #########################################################################################################
def main():
    for target_voltage_orig in TARGET_VOLTAGES:
        # add 0.5dB offset
        target_voltage = dbToGainFactor(DB_OFFSET) * target_voltage_orig
        # find actual resistor value
        mul = target_voltage / U_INPUT
        R = (mul * R_PAR) - R_PAR
        # find next matching resistor in E-Series
        dev = 999999999
        Rres = 0
        for Re in E_RESISTOR_SERIES:
            if Re < R:
                continue
            tmp_dev = abs(R - Re)
            if tmp_dev < dev:
                dev = tmp_dev
                Rres = Re
        print("Voltage range: %0.2fV (+0.5dB: %0.2fV)" % (target_voltage_orig, target_voltage))
        print("  R: %0.3fOhms" % R)
        print("  R(E-Series): %0.2fOhms" % Rres)
        print("  Actual voltage: %0.3fV" % ((Rres + R_PAR) / R_PAR * U_INPUT))
        print()


if __name__ == '__main__':
    main()
